/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-togglelogin',
  templateUrl: './togglelogin.component.html',
  styleUrls: ['./togglelogin.component.scss']
})
export class ToggleloginComponent implements OnInit {

  constructor(public modalCtrl: ModalController, public service: DataService, public route: Router) { }

  ngOnInit() {
  }
  toggleContentClose() {
    this.modalCtrl.dismiss();
  }
  gotAccountrecovery() {
    this.modalCtrl.dismiss();
    this.route.navigate(['accountrecovery']);
  }
  loginWithPhone() {
    this.modalCtrl.dismiss();
    this.route.navigate(['loginphone']);

  }
}
