/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { ToggleloginComponent } from '../togglelogin/togglelogin.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  slideOpts = {
    effect: 'flip',
    autoplay: {
      delay: 2000
    }
  };
  showContent: any;
  data: any = [];
  constructor(public route: Router, public service: DataService) {
    this.showContent = true;
    this.data = [{
      quote: 'Discover new and interestind people nearby',
      image: '../../assets/chat/chat1.jpg'
    }, {
      quote: 'Swipe right to like some one and swipe left to pass',
      image: '../../assets/chat/like.jpeg'
    }, {
      quote: 'If they also swipe right, It\'s a match!',
      image: '../../assets/chat/match2.jpg'
    }, {
      quote: 'Only people you\'ve matched can message you',
      image: '../../assets/chat/rose.png'
    }];
  }

  ngOnInit() {
  }
  toggleContent() {
    // this.showContent = !this.showContent
    this.service.openModal(ToggleloginComponent, '', 'modalBackground');
  }
  loginWithPhone() {
    this.route.navigate(['loginphone']);
  }
  gotAccountrecovery() {
    this.route.navigate(['accountrecovery']);
  }
}
