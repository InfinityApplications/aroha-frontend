/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { TermsComponent } from '../terms/terms.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  clicked: boolean;
  rangeSingle = 10;
  rangedualKnobs = { lower: 18, upper: 52 };
  click: any;
  constructor(public route: Router, public service: DataService) {
    this.clicked = false;
  }

  ngOnInit() {
  }
  showNewLocation() {
    this.clicked = true;
  }
  gotofeedPage() {
    this.route.navigate(['feed']);
  }
  gotoautoplayvideoPage() {
    this.route.navigate(['autoplayvideo']);
  }
  gotoEmailverification() {
    this.route.navigate(['emailverification']);
  }
  gotoPush() {
    this.route.navigate(['pushnotification']);
  }
  gotoDeleteAccount() {
    this.route.navigate(['deleteaccount']);
  }
  gotophoneNumber() {
    this.route.navigate(['phonenumber']);
  }
  gotoshowme() {
    this.route.navigate(['showme']);
  }
  changeColor(value) {
    if (value === 'km') {
      this.click = true;
    } else if (value === 'Mi') {
      this.click = true;
    }

  }

  gotoTeam() {
    this.route.navigate(['teamtinder']);
  }
  gotoLegal() {
    this.service.openModal(TermsComponent, '');
  }
  gotoFunctions() { }
}
