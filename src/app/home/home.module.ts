/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { HttpClientModule } from '@angular/common/http';
import { SwingModule } from 'angular2-swing';
import { IonicSwingModule } from '../ionic-swing/ionic-swing.module';
import { ChatComponent } from '../chat/chat.component';
import { ContactsComponent } from '../contacts/contacts.component';
import { MessagefeedComponent } from '../messagefeed/messagefeed.component';
import { EditPage} from '../edit/edit.page'
import {SettingsPage} from '../settings/settings.page'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    SwingModule,
    IonicSwingModule,
     RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, ChatComponent, ContactsComponent, MessagefeedComponent, EditPage,SettingsPage]
})
export class HomePageModule { }



