/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { IonSlides, IonContent } from '@ionic/angular';
import { DataService, HomeTab } from '../data.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

import { SwingCardDirective, SwingStackDirective, Direction, ThrowEvent, DragEvent, StackConfig } from '../ionic-swing/ionic-swing.module';
import { TindericonsComponent } from '../tindericons/tindericons.component';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  names = [
    {name: 'one'},
    {name: 'two'},
    {name: 'three'},
    {name: 'four'},
    {name: 'five'},
  ]
  tabs =[
    { url: '../../assets/icon/user-icon.svg', title: 'contact' },
    { url: '../../assets/icon/Aroha-Icon.svg', title: 'flame' },
    { url: '../../assets/icon/message-icon.svg', title: 'message' }
  ]
  flameTabs  = [
    { url: '../../assets/icon/user-icon.svg', title: 'contact' },
    { url: '../../assets/icon/Aroha-Icon.svg', title: 'flame' },
    { url: '../../assets/icon/message-icon.svg', title: 'message' }
  ];

  messageTabs= [
    { url: '../../assets/icon/user-icon.svg', title: 'contact' },
    { url: '../../assets/icon/Aroha-Icon.svg', title: 'flame' },
    { url: '../../assets/icon/message-icon.svg', title: 'message' },
    { url: '../../assets/icon/cafe-outline.svg', title: 'cafe' },
    { url: '../../assets/icon/vr.svg', title: 'vr' },
  ];

  profileTabs = [
    { url: '../../assets/icon/user-icon.svg', title: 'contact' },
    { url: '../../assets/icon/Aroha-Icon.svg', title: 'flame' },
    { url: '../../assets/icon/message-icon.svg', title: 'message' },
    { url: '../../assets/icon/create-outline.svg', title: 'edit' },
    { url: '../../assets/icon/settings-outline.svg', title: 'settings' },
  ];
  segmentsPerRow = 3
  rows = Array.from(Array(Math.ceil(this.flameTabs.length / this.segmentsPerRow)).keys())
  items = [];
  segmentButtonTitle='flame'
  @ViewChild('IonContent', { static: true }) content: IonContent;
  @ViewChild('Slides', { static: true }) slides: IonSlides;
  @ViewChild('swingStack', { static: true, read: SwingStackDirective }) swingStack: SwingStackDirective;
  @ViewChildren('swingCards', { read: SwingCardDirective }) swingCards: QueryList<SwingCardDirective>;
   slideOpts = {
  grabCursor: true,
  cubeEffect: {
    shadow: true,
    slideShadows: true,
    shadowOffset: 20,
    shadowScale: 0.94,
  },
  on: {
    beforeInit: function() {
      const swiper = this;
      swiper.classNames.push(`${swiper.params.containerModifierClass}cube`);
      swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);

      const overwriteParams = {
        slidesPerView: 1,
        slidesPerColumn: 1,
        slidesPerGroup: 1,
        watchSlidesProgress: true,
        resistanceRatio: 0,
        spaceBetween: 0,
        centeredSlides: false,
        virtualTranslate: true,
      };

      this.params = Object.assign(this.params, overwriteParams);
      this.originalParams = Object.assign(this.originalParams, overwriteParams);
    },
    setTranslate: function() {
      const swiper = this;
      const {
        $el, $wrapperEl, slides, width: swiperWidth, height: swiperHeight, rtlTranslate: rtl, size: swiperSize,
      } = swiper;
      const params = swiper.params.cubeEffect;
      const isHorizontal = swiper.isHorizontal();
      const isVirtual = swiper.virtual && swiper.params.virtual.enabled;
      let wrapperRotate = 0;
      let $cubeShadowEl;
      if (params.shadow) {
        if (isHorizontal) {
          $cubeShadowEl = $wrapperEl.find('.swiper-cube-shadow');
          if ($cubeShadowEl.length === 0) {
            $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
            $wrapperEl.append($cubeShadowEl);
          }
          $cubeShadowEl.css({ height: `${swiperWidth}px` });
        } else {
          $cubeShadowEl = $el.find('.swiper-cube-shadow');
          if ($cubeShadowEl.length === 0) {
            $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
            $el.append($cubeShadowEl);
          }
        }
      }

      for (let i = 0; i < slides.length; i += 1) {
        const $slideEl = slides.eq(i);
        let slideIndex = i;
        if (isVirtual) {
          slideIndex = parseInt($slideEl.attr('data-swiper-slide-index'), 10);
        }
        let slideAngle = slideIndex * 90;
        let round = Math.floor(slideAngle / 360);
        if (rtl) {
          slideAngle = -slideAngle;
          round = Math.floor(-slideAngle / 360);
        }
        const progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
        let tx = 0;
        let ty = 0;
        let tz = 0;
        if (slideIndex % 4 === 0) {
          tx = -round * 4 * swiperSize;
          tz = 0;
        } else if ((slideIndex - 1) % 4 === 0) {
          tx = 0;
          tz = -round * 4 * swiperSize;
        } else if ((slideIndex - 2) % 4 === 0) {
          tx = swiperSize + (round * 4 * swiperSize);
          tz = swiperSize;
        } else if ((slideIndex - 3) % 4 === 0) {
          tx = -swiperSize;
          tz = (3 * swiperSize) + (swiperSize * 4 * round);
        }
        if (rtl) {
          tx = -tx;
        }

         if (!isHorizontal) {
          ty = tx;
          tx = 0;
        }

         const transform$$1 = `rotateX(${isHorizontal ? 0 : -slideAngle}deg) rotateY(${isHorizontal ? slideAngle : 0}deg) translate3d(${tx}px, ${ty}px, ${tz}px)`;
        if (progress <= 1 && progress > -1) {
          wrapperRotate = (slideIndex * 90) + (progress * 90);
          if (rtl) wrapperRotate = (-slideIndex * 90) - (progress * 90);
        }
        $slideEl.transform(transform$$1);
        if (params.slideShadows) {
          // Set shadows
          let shadowBefore = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
          let shadowAfter = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');
          if (shadowBefore.length === 0) {
            shadowBefore = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'left' : 'top'}"></div>`);
            $slideEl.append(shadowBefore);
          }
          if (shadowAfter.length === 0) {
            shadowAfter = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'right' : 'bottom'}"></div>`);
            $slideEl.append(shadowAfter);
          }
          if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
          if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
        }
      }
      $wrapperEl.css({
        '-webkit-transform-origin': `50% 50% -${swiperSize / 2}px`,
        '-moz-transform-origin': `50% 50% -${swiperSize / 2}px`,
        '-ms-transform-origin': `50% 50% -${swiperSize / 2}px`,
        'transform-origin': `50% 50% -${swiperSize / 2}px`,
      });

       if (params.shadow) {
        if (isHorizontal) {
          $cubeShadowEl.transform(`translate3d(0px, ${(swiperWidth / 2) + params.shadowOffset}px, ${-swiperWidth / 2}px) rotateX(90deg) rotateZ(0deg) scale(${params.shadowScale})`);
        } else {
          const shadowAngle = Math.abs(wrapperRotate) - (Math.floor(Math.abs(wrapperRotate) / 90) * 90);
          const multiplier = 1.5 - (
            (Math.sin((shadowAngle * 2 * Math.PI) / 360) / 2)
            + (Math.cos((shadowAngle * 2 * Math.PI) / 360) / 2)
          );
          const scale1 = params.shadowScale;
          const scale2 = params.shadowScale / multiplier;
          const offset$$1 = params.shadowOffset;
          $cubeShadowEl.transform(`scale3d(${scale1}, 1, ${scale2}) translate3d(0px, ${(swiperHeight / 2) + offset$$1}px, ${-swiperHeight / 2 / scale2}px) rotateX(-90deg)`);
        }
      }

      const zFactor = (swiper.browser.isSafari || swiper.browser.isUiWebView) ? (-swiperSize / 2) : 0;
      $wrapperEl
        .transform(`translate3d(0px,0,${zFactor}px) rotateX(${swiper.isHorizontal() ? 0 : wrapperRotate}deg) rotateY(${swiper.isHorizontal() ? -wrapperRotate : 0}deg)`);
    },
    setTransition: function(duration) {
      const swiper = this;
      const { $el, slides } = swiper;
      slides
        .transition(duration)
        .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
        .transition(duration);
      if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
        $el.find('.swiper-cube-shadow').transition(duration);
      }
    },
  }
}



  segment = '';
  index = 0;
  data: Array<HomeTab> = [];

  segmentButton: any = 'flame';
  userDetail: { userDetails: string; }[];
  cards: Array<any>;
  stackConfig: StackConfig;
  recentCard = '';
  slidesImg: { image: string; }[];
  footerIcon: HomeTab[];
  subSegmentButton: any;
  modalData: any;
  modalstarData: any;
  modalFlashData: any;
  modalRefreshData: any;
  showButton: boolean;
  modalGold: any;
  hasUserData: any;
  conversation = [
    { text: 'Hey, that\'s an awesome chat UI', sender: 0, image: 'assets/images/sg2.jpg' },
    { text: 'Right, it totally blew my mind', sender: 1, image: 'assets/images/sg1.jpg', read: true, delivered: true, sent: true },
    { text: 'And it is free ?', sender: 0, image: 'assets/images/sg2.jpg' },
    { text: 'Yes, totally free', sender: 1, image: 'assets/images/sg1.jpg', read: true, delivered: true, sent: true },
    { text: 'Wow, that\'s so cool', sender: 0, image: 'assets/images/sg2.jpg' },
    { text: 'Hats off to the developers', sender: 1, image: 'assets/images/sg1.jpg', read: false, delivered: true, sent: true },
    { text: 'Oh yes, this is gooood stuff', sender: 0, image: 'assets/images/sg2.jpg' },
    { text: 'Check out their other designs ', sender: 1, image: 'assets/images/sg1.jpg', read: false, delivered: false, sent: true }
  ];
  phone_model = 'iPhone';
  input = '';
  like: boolean;
  disLike: boolean;
  superLike: boolean;
  nope: boolean;
  clicked: any;
  constructor(
    public dataService: DataService,
    public route: Router,
    public alertController: AlertController) {
    this.data = dataService.tabs;
    this.userDetail = dataService.details;
    this.cards = dataService.card;
    this.footerIcon = dataService.footer_icons;
    this.modalstarData = dataService.star;
    this.modalFlashData = dataService.flash;
    this.modalRefreshData = dataService.refresh;
    this.modalGold = dataService.gold;
    this.showButton = true;
    this.like = false;
    this.disLike = false;
    this.superLike = false;
    this.clicked = true;
    this.hasUserData = false;
    for (let i = 0; i < 30; i++) {
      this.items.push( this.items.length );
    }
    this.stackConfig = {
      
      allowedDirections: [Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT],
      throwOutConfidence: (offsetX, offsetY, element) => {
        return Math.min(Math.max(Math.abs(offsetX) / (element.offsetWidth / 1.7), Math.abs(offsetY) / (element.offsetHeight / 2)), 1);
      },

      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },
      throwOutDistance: (d) => {
        return 800;
      }
    };
    setTimeout(() => {
      this.hasUserData = true;
    }, 2000);
  }

  clikedIconIs(icon) {
    if (icon === 'refresh') {
      this.dataService.openModal(TindericonsComponent, this.modalRefreshData);
    } else if (icon === 'close') {
      this.disLike = true;
      setTimeout(() => {
        this.disLike = false;
        this.cards.pop();
      }, 200);
    } else if (icon === 'star') {
      this.superLike = true;
      setTimeout(() => {
        this.superLike = false;
        this.cards.pop();
      }, 200);
    } else if (icon === 'heart') {
      this.like = true;
      setTimeout(() => {
        this.like = false;
        this.cards.pop();
      }, 200);
    } else if (icon === 'star') {
      this.dataService.openModal(TindericonsComponent, this.modalstarData, 'modalBackground');
    } else if (icon === 'flash') {
      this.dataService.openModal(TindericonsComponent, this.modalFlashData, 'modalBackground');
    } else if (icon === 'star') {
      this.dataService.openModal(TindericonsComponent, this.modalstarData, 'modalBackground');
    }
  }



  updateImage(i) {
  }
  onItemMove(element, x, y, r) {
    const color = '';
    const abs = Math.abs(x);
    const min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));
    const hexCode = this.decimalToHex(min, 2);
    element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }

  voteUp(like: boolean) {
    const removedCard = this.cards.pop();
    this.addNewCards;
    if (like) {
      this.recentCard = 'You liked: ' + removedCard.name;
    } else {
      this.recentCard = 'You disliked: ' + removedCard.name;
    }
  }

  addNewCards(count: number) {
    // this.cards.push(this.recentCard);
  }

  decimalToHex(d, padding) {
    let hex = Number(d).toString(16);
    padding = typeof (padding) === 'undefined' || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
      hex = '0' + hex;
    }

    return hex;
  }



  segmentChanged(event: any) {
    if(event.detail.value == 'flame'){
      this.rows = Array.from(Array(Math.ceil(this.flameTabs.length / this.segmentsPerRow)).keys())
      this.tabs=this.flameTabs;
    }
    else if(event.detail.value == 'message' ||event.detail.value == 'vr' || event.detail.value == 'cafe'){
      this.rows = Array.from(Array(Math.ceil(this.messageTabs.length / this.segmentsPerRow)).keys())
      this.tabs=this.messageTabs;
    }
    else if(event.detail.value == 'contact' ||event.detail.value == 'settings' || event.detail.value == 'edit'){
      this.rows = Array.from(Array(Math.ceil(this.profileTabs.length / this.segmentsPerRow)).keys())
      this.tabs=this.profileTabs;
    }
    
    this.segmentButton = event.detail.value;
    this.segmentButtonTitle = event.detail.value;
    console.log(this.segmentButtonTitle)
  }
  openandHideDetail(userData: any) {
    this.route.navigate(['profiledetails', { userData: JSON.stringify(userData) }]);
    console.log(userData);

  }


  async change() {
    await this.slides.getActiveIndex().then(data => this.index = data);
    if (this.index === 0) {
      this.showButton = true;
    } else if (this.index !== 0) {
      this.showButton = false;
    }
  }

  onDragMove(event: DragEvent){
    if (event.offset > 5 || event.throwDirection === 'Symbol(RIGHT)') {
      this.like = true;
      this.disLike = false
      this.superLike = false

    } else if (event.offset < -5 || event.throwDirection === 'Symbol(LEFT)') {
      this.like = false
      this.disLike = true
      this.superLike = false
    } else if (event.offset > 0 || event.throwDirection === 'Symbol(UP)') {
      this.like = false
      this.disLike = false
      this.superLike = true
    }
  }

  
  onDragEnd(event: DragEvent){
    this.like = false;
    this.disLike = false;
    this.superLike = false;
  }

  doRefresh(event) {

    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onThrowOut(event: ThrowEvent) {
  }

  send() {
    if (this.input !== '') {
      this.conversation.push({ text: this.input, sender: 1, image: 'assets/images/sg1.jpg' });
      this.input = '';
      setTimeout(() => {
        this.content.scrollToBottom(50);
      }, 200);
    }
  }
  something($event: any) {
    $event.preventDefault();
  }

}



