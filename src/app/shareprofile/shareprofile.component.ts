/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-shareprofile',
  templateUrl: './shareprofile.component.html',
  styleUrls: ['./shareprofile.component.scss']
})
export class ShareprofileComponent implements OnInit {

  constructor(public popCtrl: PopoverController) { }
  closePopOver() {
    this.popCtrl.dismiss();
  }
  ngOnInit() {
  }

}
