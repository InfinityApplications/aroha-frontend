/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  data: any;
  imageData: any;
  height:any;
  allimages:["../../assets/images/img.jpg"]
  constructor(public serviceProvider: DataService, platform: Platform) {
    this.data = serviceProvider.editInfo;
    this.imageData = serviceProvider.images;
    platform.ready().then(() => {
      console.log('Height: ' + platform.height());
      this.height=platform.height()+'px';
    });
  }

  ngOnInit() {
  }

}
