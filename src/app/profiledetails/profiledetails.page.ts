/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { NavController, PopoverController, Platform } from '@ionic/angular';
import { ShareprofileComponent } from '../shareprofile/shareprofile.component';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-profiledetails',
  templateUrl: './profiledetails.page.html',
  styleUrls: ['./profiledetails.page.scss'],
})
export class ProfiledetailsPage implements OnInit {
  isIos: boolean;
  profileData: any;
  icons: any;
  slideOpts = {
    effect: 'flip',
    direction: 'horizontal',
    scrollbar: {
      el: '.swiper-scrollbar',
      draggable: true,
    },
    // autoplay: {
    //   delay: 2000,
    // }
  };
  slidesData: any;
  name: any;
  age: any;
  occupation: any;
  distance: any;
  quote: any;
  constructor(public activRouter: ActivatedRoute, public service: DataService, public navCtrl: NavController, public popOver: PopoverController, public platform: Platform) {
    this.isIos = this.platform.is('ios');
    this.icons = service.footer_icons;
    this.activRouter.params.subscribe((params) => {
      this.profileData = JSON.parse(params.userData);
      this.slidesData = this.profileData.slides;
      this.name = this.profileData.name;
      this.age = this.profileData.age;
      this.occupation = this.profileData.occupation;
      this.distance = this.profileData.distance;
      this.quote = this.profileData.quote;
      console.log('param data coming from home', this.slidesData);
    });
  }
  goBack() {
    this.navCtrl.back();
  }
  ngOnInit() {
  }
  async openPopoverOptions(ev) {
    const popover = await this.popOver.create({
      component: ShareprofileComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  goforReport() {
    this.service.openModal(ReportComponent, '');
  }
  async change(ev: any) {
    // await this.slides.getActiveIndex().then(data => this.index = data);
    // // this.segment = this.data[this.index].title;
    // // this.drag();
    // if (this.index === 0) {
    //   this.showButton = true
    // } else if (this.index !== 0) {
    //   this.showButton = false
    // }
  }
}
