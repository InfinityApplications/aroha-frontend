/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teamtinder',
  templateUrl: './teamtinder.page.html',
  styleUrls: ['./teamtinder.page.scss'],
})
export class TeamtinderPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
