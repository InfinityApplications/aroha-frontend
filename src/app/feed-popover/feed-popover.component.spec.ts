/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedPopoverComponent } from './feed-popover.component';

describe('FeedPopoverComponent', () => {
  let component: FeedPopoverComponent;
  let fixture: ComponentFixture<FeedPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
