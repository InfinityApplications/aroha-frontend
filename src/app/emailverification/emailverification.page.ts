/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-emailverification',
  templateUrl: './emailverification.page.html',
  styleUrls: ['./emailverification.page.scss'],
})
export class EmailverificationPage implements OnInit {
  emailData: any;
  constructor(public service: DataService) {
    this.emailData = service.emailVerification;
   }

  ngOnInit() {
  }

}
