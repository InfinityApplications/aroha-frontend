/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ChatstartComponent } from '../chatstart/chatstart.component';
import { DataService } from '../data.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  chatData: any;
  segmentTab: any;
  buttonClicked: boolean;
  clickData: any;
  imageData: any;
  constructor(public route: Router, public modalCtrl: ModalController, public service: DataService) {
    this.chatData = service.usersMutualTaps;
    console.log(service.usersMutualTaps);
    this.imageData = service.images;
  }

  ngOnInit() {
  }
  changeClick() {
    this.buttonClicked = !this.buttonClicked;
  }
  changeSelected() {
    setTimeout(() => {
      this.buttonClicked = false;
    }, 500);
  }
  async goforChat(chat) {
    const modal = await this.modalCtrl.create({
      component: ChatstartComponent,
      componentProps: { value: chat }
    });
    return await modal.present();
  }
}
