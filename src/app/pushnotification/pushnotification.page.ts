/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-pushnotification',
  templateUrl: './pushnotification.page.html',
  styleUrls: ['./pushnotification.page.scss'],
})
export class PushnotificationPage implements OnInit {
  pushData: any;
  constructor(public service: DataService) {
    this.pushData = service.pushNotifications;
   }

  ngOnInit() {
  }

}
