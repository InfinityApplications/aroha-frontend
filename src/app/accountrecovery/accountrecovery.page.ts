/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accountrecovery',
  templateUrl: './accountrecovery.page.html',
  styleUrls: ['./accountrecovery.page.scss'],
})
export class AccountrecoveryPage implements OnInit {

  constructor(public route: Router) { }

  ngOnInit() {
  }
  loginWithemail() {
    this.route.navigate(['loginwithemail']);
  }
}
