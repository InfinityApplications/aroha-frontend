/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { NavController, PopoverController } from '@ionic/angular';
import { ShareprofileComponent } from '../shareprofile/shareprofile.component';
import { ReportComponent } from '../report/report.component';
import { FeedPopoverComponent } from '../feed-popover/feed-popover.component';
import { ChatstartComponent } from '../chatstart/chatstart.component';
@Component({
  selector: 'app-messagefeed',
  templateUrl: './messagefeed.component.html',
  styleUrls: ['./messagefeed.component.scss']

})

export class MessagefeedComponent implements OnInit {
  profileData: any;
  icons: any;
  userDetail: { userDetails: string; }[];
  dateData : [{
    name:"Enapped",
    age:19,
    imgURL:"../../assets/chat/chat1.jpg"
    when:"05/12/2020 5:00pm",
    where:"Where : Cuba Cafe, Wellington"
  },
  {
    name:"Cassy",
    age:19,
    imgURL:"../../assets/chat/chat1.jpg",
    when:"05/12/2020 9:00pm",
    where:"Where : Clubs, Wellington"
  }]
  slideOpts = {
    effect: 'flip',
    direction: 'horizontal',
    scrollbar: {
      el: '.swiper-scrollbar',
      draggable: true,
    },
  };
  constructor(public activRouter: ActivatedRoute,
    public service: DataService,
    public navCtrl: NavController,
    public popOver: PopoverController) {
    this.userDetail = service.details;
    this.icons = service.footer_icons;
    this.activRouter.params.subscribe((params) => {
      console.log(params.userData);
      if (params.userData) {
        this.profileData = JSON.parse(params.userData);
      }
    });
  }
  goBack() {
    this.navCtrl.back();
  }
  ngOnInit() {
  }
  async openPopoverOptions(ev) {
    const popover = await this.popOver.create({
      component: ShareprofileComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  goforReport() {
    this.service.openModal(ReportComponent, '');
  }
  async change() {

  }
  async openPopover(ev) {
    const popover = await this.popOver.create({
      component: FeedPopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  openchatComponent() {
    this.service.openModal(ChatstartComponent, '');
  }
}
