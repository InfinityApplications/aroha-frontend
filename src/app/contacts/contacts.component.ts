/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { TindericonsComponent } from '../tindericons/tindericons.component';
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  userDetail: { userDetails: string; }[];
  modalGold: any;
  modalRefreshData: any;
  @ViewChild('Slides', { static: true }) slides: IonSlides;
  slideOpts = {
    effect: 'flip',
    direction: 'horizontal',
    autoplay: {
      delay: 2000,
    }
  };
  index = 0;
  showButton: boolean;

  constructor(public dataService: DataService,
    public route: Router) {
    this.userDetail = dataService.details;
    this.modalGold = dataService.gold;
    this.modalRefreshData = dataService.refresh;
    this.showButton = true;
  }

  ngOnInit() {
  }
  openGoldModal() {
    this.dataService.openModal(TindericonsComponent, this.modalGold);
  }
  opentinderPlusModal() {
    this.dataService.openModal(TindericonsComponent, this.modalRefreshData);
  }
  gotoFunctions(data: any) {
    if (data === 'create') {
      this.route.navigate(['edit']);
    } else if (data === 'settings') {
      this.route.navigate(['settings']);
    }
  }
  async change() {
    await this.slides.getActiveIndex().then(data => this.index = data);
    if (this.index === 0) {
      this.showButton = true;
    } else if (this.index !== 0) {
      this.showButton = false;
    }
  }
  gotoaddMedia() {
    this.route.navigate(['addmedia']);
  }
}
