/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams, Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tindericons',
  templateUrl: './tindericons.component.html',
  styleUrls: ['./tindericons.component.scss']
})
export class TindericonsComponent implements OnInit {
  data: any;
  slideOpts = {
    effect: 'flip'
  };
  @Input() value: any;
  show: boolean;
  isIos: boolean;
  constructor(public modalCtrl: ModalController,
    public navParams: NavParams,
    public route: Router,
    public platform: Platform) {
    this.data = this.navParams.get('value');
    this.show = false;
    this.isIos = this.platform.is('ios');

  }

  ngOnInit() {
  }
  closeModal(id: any) {
    this.modalCtrl.dismiss();
    if (id === 'star' || 'refresh' && id !== 'flash') {
      this.route.navigate(['tinderplus']);
    }
  }
  showcustomButton(index: any) {
    if (index === 1) {
      this.show = true;
    } else if (index === 0 || 2) {
      this.show = false;
    }
  }
}
