/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'tinderplus', loadChildren: './tinderplus/tinderplus.module#TinderplusPageModule' },
  { path: 'edit', loadChildren: './edit/edit.module#EditPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'feed', loadChildren: './feed/feed.module#FeedPageModule' },
  { path: 'autoplayvideo', loadChildren: './autoplayvideo/autoplayvideo.module#AutoplayvideoPageModule' },
  { path: 'pushnotification', loadChildren: './pushnotification/pushnotification.module#PushnotificationPageModule' },
  { path: 'deleteaccount', loadChildren: './deleteaccount/deleteaccount.module#DeleteaccountPageModule' },
  { path: 'deleteconfirm', loadChildren: './deleteconfirm/deleteconfirm.module#DeleteconfirmPageModule' },
  { path: 'profiledetails', loadChildren: './profiledetails/profiledetails.module#ProfiledetailsPageModule' },
  { path: 'phonenumber', loadChildren: './phonenumber/phonenumber.module#PhonenumberPageModule' },
  { path: 'showme', loadChildren: './showme/showme.module#ShowmePageModule' },
  { path: 'addmedia', loadChildren: './addmedia/addmedia.module#AddmediaPageModule' },
  { path: 'accountrecovery', loadChildren: './accountrecovery/accountrecovery.module#AccountrecoveryPageModule' },
  { path: 'teamtinder', loadChildren: './teamtinder/teamtinder.module#TeamtinderPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule'},
  { path: 'signup', loadChildren: './pages/signup/signup.module#SignupPageModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
