/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { TindericonsComponent } from './tindericons/tindericons.component';
import { ShareprofileComponent } from './shareprofile/shareprofile.component';
import { ChatstartComponent } from './chatstart/chatstart.component';
import { FormsModule } from '@angular/forms';
import { ReportComponent } from './report/report.component';
import { FeedPopoverComponent } from './feed-popover/feed-popover.component';
import { MatchComponent } from './match/match.component';
import { RefreshComponent } from './refresh/refresh.component';
import { AccountdeletereasonComponent } from './accountdeletereason/accountdeletereason.component';
import { TermsComponent } from './terms/terms.component';
import { Device } from '@ionic-native/device/ngx';

@NgModule({
  declarations: [AppComponent,
    TindericonsComponent,
    ShareprofileComponent,
    ChatstartComponent,
    ReportComponent,
    FeedPopoverComponent,
    MatchComponent,
    RefreshComponent,
    AccountdeletereasonComponent,
    TermsComponent],
  entryComponents: [TindericonsComponent,
    ShareprofileComponent,
    ChatstartComponent,
    ReportComponent,
    FeedPopoverComponent,
    MatchComponent,
    TermsComponent,
    RefreshComponent,
    AccountdeletereasonComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

