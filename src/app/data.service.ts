/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */


import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
export interface HomeTab {
  title: string;
  url: string;
}

export interface NotificationsCard {
  image: string;
  title: string;
  time: number;
}

export interface Notification {
  all: Array<NotificationsCard>;
  deals: Array<NotificationsCard>;
  orders: Array<NotificationsCard>;
  others: Array<NotificationsCard>;
}

export interface Product {
  name: string;
  image: Array<string>;
  size: string;
  color: string;
  cost_price: number;
  discount: number;
  offer: boolean;
  stock: number;
  description: string;
  currency: string;
  bought: number;
  shipping: number;
  rating: number;
  rating_count: number;
  store_rate: number;
  store_rating: number;
  store_rating_count: number;
  sold_by: string;
  specs: string;
  reviews: Array<Review>;
  store_reviews: Array<Review>;
  sizing: {
    small: number,
    okay: number,
    large: number
  };
  buyer_guarantee: string;
  sponsored: Array<Product>;
}
export interface Review {
  image: string;
  name: string;
  comment: string;
  rating: number;
  images: Array<string>;
}
export interface Cart {
  product: Product;
  quantity: number;
}

export interface User {
  fname: string;
  lname: string;
  email: string;
  address: Array<Address>;
  billing: Array<any>;
  uid: string;
  did: string;
  aid: string;
}

export interface Address {
  first_name: string;
  last_name: string;
  address_line_1: string;
  address_line_2: string;
  country: string;
  state: string;
  city: string;
  zipcode: number;
  phone_number: number;
}

export interface Orders {
  product: Product;
  order_date: Date;
  id: string;
  amount: number;
  delivery_date: Date;
  status: string;
  billing_address: Address;
  shipping_address: Address;
  tax: number;
}

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(public modalCtrl: ModalController, private http: HttpClient) { }
  
  like(userID,token) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  dislike(userID,token) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  getMatches(token) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  getStack(token, preference) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  getProfileData(token) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  login(username, password) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  logout(token) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }
  signup(data) {
    this.http.get('https://localhost:3333/posts').subscribe((response) => {
      console.log(response);
    });
  }



  details = [{
    userDetails: 'A BITS and IIT Delhi allumni established web and mobile development product and service providing company located at jaipur and bangalore.',
    image: '../../assets/chat/chat1.jpg',
    ovalContents: [{
      icon: 'settings',
      text: 'SETTINGS',
    },
    {
      icon: 'create',
      text: 'EDIT INFO',
    }],
    slides: [{
      icon: '../../assets/icon/geo.svg',
      head: 'Explore single kiwis in other areas of Aotearoa ',
      subhead: 'Change your location with Kiwi Aroha Premium!',
      color: 'primary'
    }, {
      icon: '../../assets/icon/vr.svg',
      head: 'start organising your dates with virtual reality',
      subhead: 'get early bird access to VR dating with Kiwi Aroha Premium',
      color: 'warning'
    }, {
      icon: '../../assets/icon/Aroha-Icon.svg',
      head: 'dont be limited by the daily cubes',
      subhead: 'Get unlimited cubes with Kiwi Aroha Premium!',
      color: 'gold'
    }
    ]
  }
  ];

  tabs: Array<HomeTab> = [
    { url: '../../assets/icon/user-icon.svg', title: 'contact' },
    { url: '../../assets/icon/Aroha-Icon.svg', title: 'flame' },
    { url: '../../assets/icon/message-icon.svg', title: 'message' },
    { url: '../../assets/icon/vr.svg', title: 'vr' },
    { url: '../../assets/icon/cafe-outline.svg', title: 'cafe' },
    { url: '../../assets/icon/create-outline.svg', title: 'edit' },
    { url: '../../assets/icon/settings-outline.svg', title: 'settings' },
  ];
  

  footer_icons: any = [];
  card = [
    {
      name: 'Kate Winslet',
      symbol: 'San Francisco',
      image: '../../assets/chat/chat1.jpg',
      age: '24',
      distance: 'less than a mile away',
      quote: 'Life is unfair',
      occupation: 'artist',

      slides: [
        { slideImage: '../../assets/chat/chat1.jpg' },
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    },
    {
      name: 'Jennifer Lawrence',
      symbol: 'Havard Russia',
      image: '../../assets/chat/chat2.jpg',
      age: '26',
      distance: 'less than half a mile away',
      quote: 'Life is unfair',
      occupation: 'Singer',

      slides: [
        { slideImage: '../../assets/chat/chat2.jpg' },
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    },
    {
      name: 'Jennifer Aniston',
      symbol: 'Cambridge calcutta',
      image: '../../assets/chat/chat3.jpg',
      age: '21',
      distance: 'less than one mile away',
      quote: 'Every thing is possible',
      occupation: 'Painter',

      slides: [
        { slideImage: '../../assets/chat/chat3.jpg' }
        , { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    },
    {
      name: 'Nicole ',
      symbol: 'Kawana Meghalya',
      image: '../../assets/chat/chat4.jpg',
      age: '19',
      distance: '3 mile away',
      quote: 'Life is unfair',
      occupation: 'Carpenter',

      slides: [
        { slideImage: '../../assets/chat/chat4.jpg' },
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    },
    {
      name: 'Kendrick',
      symbol: 'SentPaul',
      image: '../../assets/chat/chat5.jpg',
      age: '24',
      distance: 'less than a mile away',
      quote: 'Life is unfair',
      occupation: 'Actor',

      slides: [
        { slideImage: '../../assets/chat/chat5.jpg' },
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    },
    {
      name: 'Anna Faris',
      symbol: 'IIIME',
      image: '../../assets/chat/chat7.jpg',
      age: '26',
      distance: 'less than 3 mile away',
      quote: 'Life is unfair',
      occupation: 'Software Engineer',

      slides: [
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat7.jpg' },
        { slideImage: '../../assets/chat/chat4.jpg' }
      ]
    }
  ];


  notifications_tab: Array<HomeTab> = [{ title: 'All', url: '' },
  { title: 'Deals', url: '' },
  { title: 'Your Orders', url: '' },
  { title: 'Other', url: '' }];

  rewards_tab: Array<HomeTab> = [{ title: 'Dashboard', url: '' },
  { title: 'Redeem', url: '' },
  { title: 'Information', url: '' }];

  gold = [{
    id: 'refresh',
    mainheader: 'Get Kiwi Aroha Premium',
    icon: 'heart',
    header: 'See who tapped you',
    subHeader: 'Message them instantly',
    button: 'continue',
    descriptionHead: 'Recurring billing, cancel anytime.',
    description: 'By pressing continue, your payment will be charged to your Google play account, and your subscription will automatically renew for the same package length at the same same price untill you cancel in settings in the play store at least 24 hours prior to the end of the current period. By pressing continue, you agree to our Privacy Policy and Terms.',
    column: [{
      month: '12',
      text: 'months',
      amount: '$5.99/mo'
    }, {
      month: '6',
      text: 'months',
      amount: '$7.99/mo',
      head: 'most popular'
    }, {
      month: '1',
      text: 'months',
      amount: '$9.99/mo'
    }],
  }];

  refresh = [{
    id: 'refresh',
    mainheader: 'Get Tinder Plus',
    icon: 'refresh',
    header: 'Unlimited Rewinds',
    subHeader: 'Go back and swipe again!',
    button: 'continue',
    descriptionHead: 'Recurring billing, cancel anytime.',
    description: 'By tapping continue, your payment will be charged to your Google play account, and your subscription will automatically renew for the same package length at the same same price untill you cancel in settings in the play store at least 24 hours prior to the end of the current period. By tspppimg continue, you agree to our Privacy Policy and Terms.',
    column: [{
      month: '12',
      text: 'months',
      amount: '$166.6/mo'
    }, {
      month: '6',
      text: 'months',
      amount: '$225.6/mo'
    }, {
      month: '1',
      text: 'months',
      amount: '$330.6/mo'
    }],
  }];

  
  usersMutualTaps=[{
    'name':"Alice",
    'image':'../../assets/chat/chat1.jpg',
    'messageOverview':"New ",
    'tier':'Basic', 'crown': '../../assets/chat/crown1.png',
    'distance' : '6',
    'dateTime': "12:11:20 12:00:12",
    'messages':[{'name': "Jane", 'message': "hi how are you?"},{'name': "Alice", 'message': "I'm good!"},{'name': "Jane", 'message': "Thats' good"}]
  },
  {
    'name':"James",
    'image':'',
    'messageOverview':"New ",
    'tier':'Basic', 'crown': '../../assets/chat/crown1.png',
    'distance' : '6',
    'dateTime': "12:10:20 12:00:12",
    'messages':[{'name': "Jane", 'message': "hi how are you?"},{'name': "James", 'message': "I'm good!"},{'name': "Jane", 'message': "Thats' good"}]
  },{
    'name':"Jack",
    'image': '../../assets/chat/chat1.jpg',
    'messageOverview':"New ",
    'tier':'Basic', 'crown': '../../assets/chat/crown1.png',
    'distance' : '6',
    'dateTime': "12:9:20 12:00:12",
    'messages':[{'name': "Jane", 'message': "hi how are you?"},{'name': "James", 'message': "I'm good!"},{'name': "Jane", 'message': "Thats' good"}]
  },{
    'name':"Jill",
    'image':'https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    'messageOverview':"New ",
    'distance' : '6',
    'tier':'Basic', 'crown': '../../assets/chat/crown1.png',
    'dateTime': "12:8:20 12:00:12",
    'messages':[{'name': "Jane", 'message': "hi how are you?"},{'name': "James", 'message': "I'm good!"},{'name': "Jane", 'message': "Thats' good"}]
  },{
    'name':"Bill",
    'image':'https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
    'messageOverview':"New ",
    'distance' : '6',
    'tier':'Basic', 'crown': '../../assets/chat/crown1.png',
    'dateTime': "12:7:20 12:00:12",
    'messages':[{'name': "Jane", 'message': "hi how are you?"},{'name': "James", 'message': "I'm good!"},{'name': "Jane", 'message': "Thats' good"}]
  }
]

  userData = [{
    userID:"100",
    userName:"katie",
    userImage: '../../assets/chat/chat1.jpg',
    userDescription: 'A BITS and IIT Delhi allumni established web and mobile development product and service providing company located at jaipur and bangalore.',
    userAge: 23,
    userBirthday:"12:12:2000",
    userEmail:"andrewgarrett1996@gmail.com",
    userPremium:false,
    userPaymentDate:"12:12:1999",
    userGender: "Man", 
    userSnapchat:"sds",
    userImages1:"https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
    userImages2:"https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
    userImages3:"https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
    userImages4:"https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
    lookingFor:"Women",
    userDistance:20,
    longitude:127.00,
    latitude:128.00,
    viewing:"Local Kiwis",
    oldestAge:29,
    youngstAge:24,
    usersDates : [{name:"Anna", When : "05/12/2020 5:00pm", Where: "Cuba Cafe, Wellington"}]
  }];


  star = [{
    id: 'star',
    mainheader: 'Get Tinder Plus',
    header: 'Unlimited Likes',
    subHeader: '11:52:45',
    image: '../../assets/chat/chat4.jpg',
    subheader1: 'Swipe right as much as you want',
    button: 'continue',
    descriptionHead: 'Recurring billing, cancel anytime.',
    description: 'By tapping continue, your payment will be charged to your Google play account, and your subscription will automatically renew for the same package length at the same same price untill you cancel in settings in the play store at least 24 hours prior to the end of the current period. By tspppimg continue, you agree to our Privacy Policy and Terms.',
    column: [{
      month: '12',
      text: 'months',
      amount: '$166.6/mo'
    }, {
      month: '6',
      text: 'months',
      amount: '$225.6/mo',
      head: 'most popular'
    }, {
      month: '1',
      text: 'months',
      amount: '$330.6/mo'
    }],
  }];

  flash = [{
    id: 'flash',
    boost: '0.1x',
    view: 'VIEWS',
    header: 'Out of Boosts!',
    subHeader: 'Be the top profile in  your area for 30 minutes to get more matches.',
    button: 'boost me',
    button1: 'get tinder plus',
    button2: '(2 free Boost every month)',
    description: 'By tapping continue, your payment will be charged to your Google play account, and your subscription will automatically renew for the same package length at the same same price untill you cancel in settings in the play store at least 24 hours prior to the end of the current period. By tspppimg continue, you agree to our Privacy Policy and Terms.',
    column: [{
      month: '10',
      text: 'Boosts',
      amount: '$165.6/ea'
    }, {
      month: '6',
      text: 'Boosts',
      amount: '$225.6/ea',
      head: 'save 25%'

    }, {
      month: '1',
      text: 'Boosts',
      amount: '$330.6/ea'
    }],
    column2: [{
      icon: 'flash',
      view: 'VIEWS',
      header: 'get 2 free Boost every month',
      subHeader: 'skip the line & get more matches!',
      button: 'continue',
      colomn: [{
        month: '12',
        text: 'months',
        amount: '$166.6/mo'
      }, {
        month: '6',
        text: 'months',
        amount: '$225.6/mo',
        head: 'MOST POPULAR'

      }, {
        month: '1',
        text: 'months',
        amount: '$330.6/mo'
      }],
    }]
  }];
  menuDropdown = [{
    name: 'Default'
  }, {
    name: 'Nearby'
  }, {
    name: 'Unopened'
  }];

  tinderPlus = [{
    image: '../../assets/chat/puzzle.png',
    head: 'Unlimited right swipes',
    subHead: 'Swipes as much as you like.',
    head1: 'unlimited likes',
    subHead1: 'Give me the ability to like as manu people as i want',
    toggle: true
  }, {
    image: '../../assets/chat/flash1.png',
    head: 'skip the line',
    subHead: 'Be the top profile in your area for 30 minutes to get more matches.',
    head1: 'tinder boost',
    subHead1: 'Give me one free boost every month',
    toggle: true
  }, {
    image: '../../assets/chat/flash2.png',
    head: 'control who you see',
    subHead: 'it\'s simple now to choose the type of people you want to see on Tinder.',
    head1: 'balanced recommendations',
    subHead1: 'See the most relevent people to you(default setting)',
    heade2: 'recently active',
    subHead2: 'See the most recently active people first',
    toggle: false
  }, {
    image: '../../assets/chat/flash3.png',
    head: 'control who see you',
    subHead: 'Only be shown to certain types of people on tinder.',
    head1: 'Standard',
    subHead1: 'Only be shown to certain types of people for individual recommendations.',
    heade2: 'only people i\'ve liked',
    subHead2: 'only people i\'ve right swiped will see me.',
    toggle: false
  }, {
    image: '../../assets/chat/protection.png',
    head: 'control your profile',
    subHead: 'make parts of your profile information invisible to other people.',
    head1: 'dont show my age',
    heade2: 'Make my distance invisible',
    toggle: true
  }, {
    image: '../../assets/chat/route.png',
    head: 'passport to any location',
    subHead: 'Match with people all around the world.Paris,Los Angles, Sydney. Go!',
    head1: 'Location',
    head1right: 'My current location',
  }, {
    image: '../../assets/chat/rewind.png',
    head: 'rewind your last swipe',
    subHead: 'Accidently swiped on someone? rewind and swipe again.',
    head1: 'Rewind last swipe',
    subHead1: 'Give me the ability torewind my last swipe',
    toggle: true
  }, {
    image: '../../assets/chat/hide.png',
    head: 'hide ads',
    subHead: 'Enjoy a completely ad free experience.',
    head1: 'hide advertisement',
    toggle: true
  }];
  images = [{ image: '../../assets/chat/chat1.jpg' }, { image: '../../assets/chat/chat2.jpg' }, { image: '../../assets/chat/chat3.jpg' }, { image: '../../assets/chat/chat4.jpg' }, { icon: 'add' }, { icon: 'add' }, { icon: 'add' }, { icon: 'add' }, { icon: 'add' }];


  editInfo = [
  
    {
      head: 'About User',
      toggle: false,
      input: 'Mechanical Engineer',
      editable: true
    },
     {
      head: 'I Am',
      toggle: false,
      input: 'Man',
      editable: true
    },{
      head: 'Searching For',
      toggle: true,
      subhead1: 'Women',
      subhead2: 'men',

    },{
      head: 'control your profile',
      toggle: true,
      subhead1: 'dont show my age',
      subhead2: 'Make my distance invisible',

    },
  ];
  shareFeed = [{
    head: 'Share My Feed',
    toggle: 'true',
    para: 'Sharing your social content will greatly increase your chances of receiving messages',
    para2: 'Shared content'
  }, {
    head: 'Photos',
    toggle: 'true'
  }, {
    head: 'Spotify Anthem',
    toggle: 'true'
  }, {
    head: 'top Spotify Artist',
    toggle: 'true'
  }, {
    head: 'Bio',
    toggle: 'true'
  }, {
    head: 'School',
    toggle: 'true'
  }, {
    head: 'work',
    toggle: 'true'
  }];
  videoAutoplay = [{
    para: 'Playing videos use more datathan displaying photos,so choose when videos autoplay here.',
    para2: 'autoplay videos'
  },
  {
    head: 'On Wifi and Mobile data.',
  }, {
    head: 'On Wifi only',
  }, {
    head: 'Never autoplay Videos',
  }];
  emailVerification = [{
    para: 'Control the emails you want to get - all of them, just the important stuff or the bare minimum.you can always unsubscribe from the bottom of email.',
  },
  {
    email: 'Enappd@Enappd.com.',
    toggle: false,
    emailDesc: 'Email not verified.Tap below to request a verification email.'
  }, {
    head: 'New Matches',
    toggle: true

  }, {
    head: 'New Messages',
    toggle: true

  }, {
    head: 'Promotions',
    toggle: true,
    des: 'I want to receive newa,updates and offers from Dating.'
  }];
  pushNotifications = [{
    head: 'New Matches',
    para: 'You just got a new match',
    toggle: true
  }, {
    head: 'Messages',
    para: 'Someone sent you a new message',
    toggle: true
  }, {
    head: 'Messages Likes',
    para: 'someone liked your message',
    toggle: true
  }, {
    head: 'super Likes',
    para: 'You have been super liked! swipe to found out by whom.',
    toggle: true
  }, {
    head: 'Top Picks',
    para: 'Your daily top Picks are ready!',
    toggle: true
  }];
  deleteAccount = [{
    color: '#FFCA28',
    image: '../../assets/chat/setting.png',
    head: 'something  ',
    head1: 'is broken'
  }, {
    color: '#AED581',
    image: '../../assets/chat/brush.png',
    head: 'i want a  ', head1: 'fresh start'
  }, {
    color: '#4FC3F7',
    image: '../../assets/chat/dislike.png',
    head: 'i don\'t like  ', head1: 'dating'
  }, {
    color: '#EC407A',
    image: '../../assets/chat/heart.png',
    head: 'i met  ', head1: 'someone'
  }, {
    color: '#9E9E9E',
    image: '../../assets/chat/cup.png',
    head: 'i need a  ', head1: 'break from'
  }, {
    color: '#E91E63',
    image: '../../assets/chat/edit.png',
    head: 'other '
  }];
  report = [{
    image: '../../assets/chat/warning.png',
    head: 'Report User',
    desc: 'Is this person bothering you?Tell us what they did.',
    items: [{
      icon: 'camera',
      detail: 'Inappropriate Photos',
      color: '#5E35B1'
    }, {
      icon: 'american-football',
      detail: 'Feels like Spam',
      color: 'orange'
    }, {
      icon: 'paw',
      detail: 'Other',
      color: '#43A047'
    }]
  }];



  async openModal(comp, props, cssClass?) {
    const modal = await this.modalCtrl.create({
      component: comp,
      componentProps: { value: props },
      cssClass: cssClass
    });
    return modal.present();
  }
}
