/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-autoplayvideo',
  templateUrl: './autoplayvideo.page.html',
  styleUrls: ['./autoplayvideo.page.scss'],
})
export class AutoplayvideoPage implements OnInit {
  videoData: any;
  constructor(public service: DataService) {
    this.videoData = service.videoAutoplay;
   }

  ngOnInit() {
  }

}
