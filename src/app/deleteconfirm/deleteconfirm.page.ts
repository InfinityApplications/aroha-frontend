/**
 * Dating https://store.enappd.com/product/dating-app-starter-ionic4-dating(Tinder Clone)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-deleteconfirm',
  templateUrl: './deleteconfirm.page.html',
  styleUrls: ['./deleteconfirm.page.scss'],
})
export class DeleteconfirmPage implements OnInit {
  deleteData: any;
  constructor(public service: DataService) {
    this.deleteData = service.deleteAccount;
  }

  ngOnInit() {
  }
  goforModalReason(item: any, index) {
  }
}
